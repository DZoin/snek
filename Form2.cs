﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    static class Constants
    {
        public const int SIZE_OF_SQUARE = 10;
        public const int FIELD_SIZE = 200;
    }

    enum Directions
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    public partial class Form2 : Form
    {
        List<Tuple<int, int>> body;
        HashSet<Tuple<int, int>> walls;
        Tuple<int, int> food_location;
        Directions dir;
        uint speed_mod;
        Random rand;
        Timer timer;
        bool ticked;

        public Form2()
        {
            walls = new HashSet<Tuple<int, int>>();
            for (int i = 0; i <= Constants.FIELD_SIZE; i += Constants.SIZE_OF_SQUARE)
            {
                walls.Add(new Tuple<int, int>(0, i));
                walls.Add(new Tuple<int, int>(i, 0));
                walls.Add(new Tuple<int, int>(Constants.FIELD_SIZE, i));
                walls.Add(new Tuple<int, int>(i, Constants.FIELD_SIZE));
            }
            body = new List<Tuple<int, int>>();
            body.Add(new Tuple<int, int>(20, 20));

            food_location = null;
            dir = Directions.RIGHT;
            rand = new Random();
            speed_mod = 5;
            ticked = false;
            InitializeComponent();
            this.KeyPress += new KeyPressEventHandler(Form2_KeyPress);
            this.Load += new EventHandler(Form2_Load);
        }

        void Form2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (ticked)                                             // Allows single change of direction between frames
            {
                switch (e.KeyChar)
                {
                    case 'w':
                        if (dir == Directions.LEFT || dir == Directions.RIGHT)
                        {
                            dir = Directions.UP;
                            ticked = false;
                            Refresh();
                        }
                        break;
                    case 's':
                        if (dir == Directions.LEFT || dir == Directions.RIGHT)
                        {
                            dir = Directions.DOWN;
                            ticked = false;
                            Refresh();
                        }
                        break;
                    case 'a':
                        if (dir == Directions.UP || dir == Directions.DOWN)
                        {
                            dir = Directions.LEFT;
                            ticked = false;
                            Refresh();
                        }
                        break;
                    case 'd':
                        if (dir == Directions.UP || dir == Directions.DOWN)
                        {
                            dir = Directions.RIGHT;
                            ticked = false;
                            Refresh();
                        }
                        break;
                }
            }
            e.Handled = true;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            timer = new Timer();
            timer.Interval = (1000 / (int)speed_mod);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            ticked = true;
            if (food_location == null)
            {
                food_location = new Tuple<int, int>(rand.Next(2, Constants.FIELD_SIZE / 10) * 10, rand.Next(2, Constants.FIELD_SIZE / 10) * 10);
                if (body.Contains(food_location))
                    food_location = null;
                Invalidate();
            }         

            if (walls.Contains(body.First()) || body.LastIndexOf(body.First()) != body.IndexOf(body.First()))
            {
                timer.Stop();
                timer.Dispose();
                const string message = "GIT GUD!";
                const string caption = "GAME OVER!";
                var result = MessageBox.Show(message, caption);
                /*
                 * Must release event handles but how?!
                 * 
                 * KeyPress -= new KeyPressEventHandler(Form2_KeyPress);
                 * Load -= new EventHandler(Form2_Load);
                 * timer.Tick -= new EventHandler(timer_Tick)
                */
                Application.Exit();
            }

            switch (dir)
            {
                case Directions.UP:
                    if (food_location == null || body.First().Item1 != food_location.Item1 || (body.First().Item2 - Constants.SIZE_OF_SQUARE) != food_location.Item2)
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1, body.First().Item2 - Constants.SIZE_OF_SQUARE)); // removing from Y makes the snake go up?!
                        body.RemoveAt(body.Count() - 1);
                    }
                    else
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1, body.First().Item2 - Constants.SIZE_OF_SQUARE));
                        food_location = null;
                    }
                    Invalidate();                            // Raises a paint event. Using Refresh() invalidates and force calls OnPaint but is slower?
                    break;
                case Directions.DOWN:

                    if (food_location == null || body.First().Item1 != food_location.Item1 || (body.First().Item2 + Constants.SIZE_OF_SQUARE) != food_location.Item2)
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1, body.First().Item2 + Constants.SIZE_OF_SQUARE)); // removing from Y makes the snake go up?!
                        body.RemoveAt(body.Count() - 1);
                    }
                    else
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1, body.First().Item2 + Constants.SIZE_OF_SQUARE));
                        food_location = null;
                    }
                    Invalidate();
                    break;
                case Directions.RIGHT:
                    if (food_location == null || body.First().Item1 + Constants.SIZE_OF_SQUARE != food_location.Item1 || (body.First().Item2) != food_location.Item2)
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1 + Constants.SIZE_OF_SQUARE, body.First().Item2)); 
                        body.RemoveAt(body.Count() - 1);
                    }
                    else
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1 + Constants.SIZE_OF_SQUARE, body.First().Item2));
                        food_location = null;
                    }
                    Invalidate();
                    break;
                case Directions.LEFT:
                    if (food_location == null || body.First().Item1 - Constants.SIZE_OF_SQUARE != food_location.Item1 || (body.First().Item2) != food_location.Item2)
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1 - Constants.SIZE_OF_SQUARE, body.First().Item2)); 
                        body.RemoveAt(body.Count() - 1);
                    }
                    else
                    {
                        body.Insert(0, new Tuple<int, int>(body.First().Item1 - Constants.SIZE_OF_SQUARE, body.First().Item2));
                        food_location = null;
                    }
                    Invalidate();
                    break;
            }
        }

        void FillRect(Graphics graphics, int x, int y, int size)
        {
            graphics.FillRectangle(Brushes.Black, new Rectangle(x, y, size, size));
        }

        void Render(Graphics graphics)
        {
            foreach (var part in body)
                FillRect(graphics, part.Item1, part.Item2, Constants.SIZE_OF_SQUARE);
            foreach (var part in walls)
                FillRect(graphics, part.Item1, part.Item2, Constants.SIZE_OF_SQUARE);
            if (food_location != null)
                FillRect(graphics, food_location.Item1, food_location.Item2, Constants.SIZE_OF_SQUARE);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Render(e.Graphics);
        }
    }
}
